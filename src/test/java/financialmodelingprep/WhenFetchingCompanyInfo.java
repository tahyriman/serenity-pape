package financialmodelingprep;

import io.restassured.RestAssured;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static net.serenitybdd.rest.SerenityRest.when;

@RunWith(SerenityRunner.class)
public class WhenFetchingCompanyInfo {

    @Before
    public void setBaseUri() {
        RestAssured.baseURI = "https://financialmodelingprep.com/api/v3/company";
    }

    @Test
    public void request_should_succeed_for_listed_companies() {
        when().
                get("https://financialmodelingprep.com/api/v3/company/profile/RHT").

                then().
                statusCode(200);
    }

    @Test
    public void should_return_company_name_and_industry() {
        when().
                get("https://financialmodelingprep.com/api/v3/company/profile/RHT").
                then().
                body("profile.industry", Matchers.equalTo("Application Software")).
                and().body("profile.companyName", Matchers.equalTo("Red Hat Inc."));
    }
}
